FROM openjdk:8-jdk-alpine
ADD  build/libs/app-itau-client-0.0.1-SNAPSHOT.jar app-itau-client-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["sh", "c", "java -jar /app-itau-client-0.0.1-SNAPSHOT.jar"]
