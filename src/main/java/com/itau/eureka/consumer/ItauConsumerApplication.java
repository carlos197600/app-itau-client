package com.itau.eureka.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.itau.eureka.consumer.controller","com.itau.eureka.consumer.config","com.itau.eureka.consumer.consumer","com.itau.eureka.consumer.service"})
public class ItauConsumerApplication {

	public static void main(String[] args) {

		SpringApplication.run(ItauConsumerApplication.class, args);
	}
}
