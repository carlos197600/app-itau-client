package com.itau.eureka.consumer.controller;

import com.itau.eureka.consumer.service.ValidadorDeTransacoesItauService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/itau-api-client")
@RestController
public class UserItauController {

    private final ValidadorDeTransacoesItauService service;

    @Autowired
    public UserItauController(ValidadorDeTransacoesItauService service) {
        this.service = service;
    }

    @GetMapping("/{codigoTransacao}")
    public String verificaTransacao(@PathVariable String codigoTransacao) {

        return service.validarTransacao(codigoTransacao);
    }

}