package com.itau.eureka.consumer.service;

import com.itau.eureka.consumer.consumer.ItauConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidadorDeTransacoesItauService {

    private final ItauConsumer itauConsumer;

    @Autowired
    public ValidadorDeTransacoesItauService(ItauConsumer itauConsumer) {
        this.itauConsumer = itauConsumer;
    }

    public String validarTransacao(String transacao) {
        String mensagem = itauConsumer.validarTransacaoSuspeitas();
        return mensagem + " : [" + transacao + "] !";
    }
}