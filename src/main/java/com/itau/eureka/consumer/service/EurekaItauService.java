package com.itau.eureka.consumer.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;

/*
 Classe reponsavel por obter as instancias no Eureka Server

 */

@Service
public class EurekaItauService {

    private final DiscoveryClient discoveryClient;

    @Autowired
    public EurekaItauService(DiscoveryClient discoveryClient) {

        this.discoveryClient = discoveryClient;
    }

}
