package com.itau.eureka.consumer.consumer;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ItauConsumer {

    private final RestTemplate restTemplate;

    @Value("${app.itauService.serviceId}")
    private String itauServiceId;

    @Value("${app.itauService.endpoint.itau}")
    private String itauEndPointUri;

    @Autowired
    public ItauConsumer(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @HystrixCommand(fallbackMethod = "getDefaultItau")
    public String validarTransacaoSuspeitas() {
        String retorno = restTemplate.getForObject("http://itau-service" + itauEndPointUri, String.class);
        return retorno;
    }

    public String getDefaultGreeting() {
        return "Sucesso...";
    }
}

